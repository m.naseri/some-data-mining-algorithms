import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.externals.six import StringIO
from sklearn.tree import export_graphviz
import pydotplus
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import numpy as np

# load dataset
dataset = pd.read_csv("./Dataset1.csv")
unknownDataset = pd.read_csv("./Dataset1_Unknown.csv")
# split dataset in features and target variable
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values
uX = unknownDataset.iloc[:, :].values

# Pre processing
# HANDLE MISSING VALUE
XALL = np.concatenate((X, uX))

labelencoder_y = LabelEncoder()
y = labelencoder_y.fit_transform(y)
labelencoder_X = LabelEncoder()
categoricalFeatures = [1, 3, 5, 6, 7, 8, 9, 13]
for i in categoricalFeatures:
    XALL[:, i] = labelencoder_X.fit_transform(XALL[:, i])

onehotencoder = OneHotEncoder(categorical_features=categoricalFeatures)
XALL = onehotencoder.fit_transform(XALL).toarray()

X = XALL[0:X.shape[0], :]
uX = XALL[X.shape[0]:, :]


# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

# Decision Trees
# clfEntropy = DecisionTreeClassifier(criterion="entropy")
clfEntropy = DecisionTreeClassifier(criterion="entropy", max_depth=7, min_samples_split=2)
clfEntropy = clfEntropy.fit(X_train, y_train)
y_pred = clfEntropy.predict(X_test)
u_y_pred_E = clfEntropy.predict(uX)
print("Accuracy of clfEntropy:", metrics.accuracy_score(y_test, y_pred))

# clfGini = DecisionTreeClassifier(criterion="gini")
clfGini = DecisionTreeClassifier(criterion="gini", max_depth=8, min_samples_split=2)
clfGini = clfGini.fit(X_train, y_train)
y_pred = clfGini.predict(X_test)
u_y_pred_G = clfGini.predict(uX)
print("Accuracy of clfGini:", metrics.accuracy_score(y_test, y_pred))

# Export predicted labels
pd.DataFrame(u_y_pred_E).to_csv("unknownY-Entropy.csv")
pd.DataFrame(u_y_pred_G).to_csv("unknownY-Gini.csv")


# Visualize decision trees
dot_data = StringIO()
export_graphviz(clfEntropy, out_file=dot_data,
                filled=True, rounded=True,
                special_characters=True, class_names=['0', '1'])
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png('D1-DT-Entropy.png')

dot_data = StringIO()
export_graphviz(clfGini, out_file=dot_data,
                filled=True, rounded=True,
                special_characters=True, class_names=['0', '1'])
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png('D1-DT-Gini.png')