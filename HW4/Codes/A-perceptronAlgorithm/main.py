from sklearn.datasets import load_breast_cancer
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import random


class Perceptron:
    def __init__(self):
        self.w = None
        self.b = 0

    def model(self, x):
        if np.dot(self.w, x) >= self.b:
            return 1
        return 0

    def predict(self, X):
        Y = []
        for x in X:
            result = self.model(x)
            Y.append(result)

        return np.array(Y)

    def fit(self, X, Y, epochs=1, lr=1.):
        self.w = np.array([random.random()
                           for k in range(X.shape[1])])
        self.b = 0

        accuracy = {}
        max_accuracy = 0

        wt_matrix = []

        for i in range(epochs):
            for x, y in zip(X, Y):
                y_pred = self.model(x)
                if y == 1 and y_pred == 0:
                    self.w = self.w + lr * x
                    self.b = self.b - lr * 1
                elif y == 0 and y_pred == 1:
                    self.w = self.w - lr * x
                    self.b = self.b + lr * 1

            wt_matrix.append([self.w, self.b])

        return np.array(wt_matrix)


# load data
data = load_breast_cancer()

X = data.data

y = data.target
y = y.reshape((y.shape[0], 1))

ds = np.concatenate((X, y), axis=1)

# balance imbalanced dataset
countOf1 = 0  # 357
countOf0 = 0  # 212
for elem in y:
    if elem == 0:
        countOf0 += 1
    elif elem == 1:
        countOf1 += 1

df = pd.DataFrame(data=ds[:, :])
shuffled_df = df.sample(frac=1, random_state=4)

negative_df = shuffled_df.loc[shuffled_df[30] == 0]
positive_df = shuffled_df.loc[shuffled_df[30] == 1].sample(n=212, random_state=42)

balanced_df = pd.concat([negative_df, positive_df])

# scale dataset
scaler = MinMaxScaler()
scaledDf = scaler.fit_transform(balanced_df)
scaledDf = pd.DataFrame(data=scaledDf[:, :])
X = scaledDf.drop(30, axis=1)
Y = scaledDf[30]

# train, test
perceptron = Perceptron()

# for learningRate in [0.001, 0.01, 0.1, 0.2, 0.4, 0.8]:
for testSize in [0.1, 0.2, 0.3, 0.4]:

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=testSize, random_state=1)
    X_train = X_train.values
    Y_train = Y_train.values
    X_test = X_test.values
    Y_test = Y_test.values

    nEpochs = 5000
    learningRate = 0.4
    wt_matrix = perceptron.fit(X_train, Y_train, nEpochs, learningRate)

    Jth = 0  # number of epoch which has best accuracy
    bestAccuracy = 0
    accuracies = []
    # performance measurement
    for i in range(len(wt_matrix)):
        perceptron.w = wt_matrix[i][0]
        perceptron.b = wt_matrix[i][1]
        Y_hat = perceptron.predict(X_test)
        accuracy = accuracy_score(Y_hat, Y_test)
        accuracies.append(accuracy)
        if accuracy > bestAccuracy:
            Jth = i
            bestAccuracy = accuracy

    # Y_hat = perceptron.predict(X_test)
    fig = plt.figure()
    plt.plot(list(accuracies))
    plt.title('accuracy vs nEpoch Random Weights lr={} testSize={}'.format(learningRate, testSize))
    plt.savefig('accuracy-nEpoch-RandomWeights-lr{}-testSize{}.png'.format(learningRate, testSize), dpi=fig.dpi)
    plt.show()

    print("bestAccuracy on test data (RANDOM WEIGHTS): {}, epoch number {}/{}, learning rate {}, testSize {}"
          .format(bestAccuracy, Jth, nEpochs, learningRate, testSize))
