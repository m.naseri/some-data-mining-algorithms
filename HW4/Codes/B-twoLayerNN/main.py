import numpy as np
import matplotlib.pyplot as plt


class NeuralNetwork:
    def __init__(self, x, y, learningRate):
        self.input = x
        self.y = y
        self.output = np.zeros(self.y.shape)
        self.outputOfLayer1 = None
        self.w1 = np.random.rand(self.input.shape[1], 4)
        self.w2 = np.random.rand(4, 1)
        self.learningRate = learningRate

    def feedForward(self):
        self.outputOfLayer1 = 1 / (1 + np.exp(-np.dot(self.input, self.w1)))
        self.output = 1 / (1 + np.exp(-np.dot(self.outputOfLayer1, self.w2)))

    def errorBackPropagation(self):
        temp = 2 * (self.y - self.output) * self.output * (1 - self.output)
        deltaW2 = np.dot(self.outputOfLayer1.T, temp)

        temp = 2 * (self.y - self.output) * self.output * (1 - self.output)
        temp = np.dot(temp, self.w2.T)
        temp *= self.outputOfLayer1 * (1 - self.outputOfLayer1)
        deltaW1 = np.dot(self.input.T, temp)

        self.w1 += learningRate*deltaW1
        self.w2 += learningRate*deltaW2

    def calculateLossFunction(self):
        SSE = 0
        for i in range(len(self.y)):
            SSE += (self.y[i] - self.output[i])**2
        return SSE


X = np.array([[0, 0, 1],
              [0, 1, 1],
              [1, 0, 1],
              [1, 1, 1]])
y = np.array([
    [0],
    [1],
    [1],
    [0]])

nOfEpoch = 2000
for learningRate in [0.001, 0.01, 0.1, 0.2, 0.4, 0.8, 1, 2, 10, 100, 1000]:
    nn = NeuralNetwork(X, y, learningRate)

    Losses = []
    for i in range(nOfEpoch):
        nn.feedForward()
        nn.errorBackPropagation()
        loss = nn.calculateLossFunction()
        Losses.append(loss)

    fig = plt.figure()
    plt.plot(Losses)
    plt.title('Loss vs iteration learningRate={}'.format(learningRate))
    plt.show()
    fig.savefig('lossVSIteration-LearningRate{}.png'.format(learningRate), dpi=fig.dpi)

    print("output of nn (learningRate = {})\n {}".format(learningRate, nn.output))
    print("final SSE: {}\n\n".format(Losses[-1]))
