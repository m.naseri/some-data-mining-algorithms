import numpy as np
from matplotlib import pyplot
import pandas as pd
from scipy import stats

ds = pd.read_csv('data.csv')
for i in range(ds['Amount'].size):
    num = ds.iloc[i]['Amount'].replace('$', '')
    num = num.replace(',', '')
    ds.set_value(i, 'Amount', (num))
    Transaction_Type = ds.iloc[i]['Transaction_Type']
    if Transaction_Type == 'Cash':
        ds.set_value(i, 'Transaction_Type', 0)
    else:
        ds.set_value(i, 'Transaction_Type', 1)

ds = ds.drop(['Day', 'BranchName', 'Month'], axis=1)
ds = ds.apply(pd.to_numeric)

ds = ds[(np.abs(stats.zscore(ds)) < 3).all(axis=1)]
print(ds)

fig = []
ax = []
for i in range(6):
    fig.append(pyplot.figure(i))
    ax.append(fig[i].gca())

ax[0].hist(ds['Amount'])
ax[0].set_xlabel('Amount')

ax[1].boxplot(ds['Amount'])
ax[1].set_xlabel('Amount')

ax[2].scatter(ds['DayWeek'], ds['Amount'])
ax[2].set_xlabel('DayWeek')
ax[2].set_ylabel('Amount')

ax[3].scatter(ds['Hour'], ds['Amount'])
ax[3].set_xlabel('Hour')
ax[3].set_ylabel('Amount')

ax[4].scatter(ds['Week'], ds['Amount'])
ax[4].set_xlabel('Week')
ax[4].set_ylabel('Amount')

ax[5].scatter(ds['Units'], ds['Amount'])
ax[5].set_xlabel('Units')
ax[5].set_ylabel('Amount')
pyplot.show()
fig[0].savefig('Amount-Histogram.png')
fig[1].savefig('Amount-BoxPlot.png')
fig[2].savefig('Amount-DayWeek-Scatter.png')
fig[3].savefig('Amount-Hour-BoxPlot.png')
fig[4].savefig('Amount-Week-Scatter.png')
fig[5].savefig('Amount-Units-BoxPlot.png')

ds.to_csv('dataOut.csv')
