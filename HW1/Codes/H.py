import numpy as np
from matplotlib import pyplot
import pandas as pd

ds = pd.read_csv('data.csv')
for i in range(ds['Amount'].size):
    num = ds.iloc[i]['Amount'].replace('$', '')
    num = float(num.replace(',', ''))
    ds.set_value(i, 'Amount', num)

ds = ds.drop(['Day', 'BranchName', 'Month'], axis=1)

fig = []
ax = []
for i in range(6):
    fig.append(pyplot.figure(i))
    ax.append(fig[i].gca())

ax[0].hist(ds['Amount'])
ax[0].set_xlabel('Amount')

ax[1].boxplot(ds['Amount'])
ax[1].set_xlabel('Amount')

ax[2].scatter(ds['DayWeek'], ds['Amount'])
ax[2].set_xlabel('DayWeek')
ax[2].set_ylabel('Amount')

ax[3].scatter(ds['Hour'], ds['Amount'])
ax[3].set_xlabel('Hour')
ax[3].set_ylabel('Amount')

ax[4].scatter(ds['Week'], ds['Amount'])
ax[4].set_xlabel('Week')
ax[4].set_ylabel('Amount')

ax[5].scatter(ds['Units'], ds['Amount'])
ax[5].set_xlabel('Units')
ax[5].set_ylabel('Amount')
pyplot.show()
fig[0].savefig('Amount-Histogram.png')
fig[1].savefig('Amount-BoxPlot.png')
fig[2].savefig('Amount-DayWeek-Scatter.png')
fig[3].savefig('Amount-Hour-BoxPlot.png')
fig[4].savefig('Amount-Week-Scatter.png')
fig[5].savefig('Amount-Units-BoxPlot.png')


# print(ds)
# ds.to_csv('dataOut.csv')
