import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ds = pd.read_csv('data.csv')
for i in range(ds['Amount'].size):
    num = ds.iloc[i]['Amount'].replace('$', '')
    num = float(num.replace(',', ''))
    ds.set_value(i, 'Amount', num)

ds = ds.drop(['Day', 'BranchName', 'Month'], axis=1)
print(ds)
ds.to_csv('dataOut.csv')