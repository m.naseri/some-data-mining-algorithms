import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ds = pd.read_csv('data.csv')
for i in range(ds['Amount'].size):
    num = ds.iloc[i]['Amount'].replace('$', '')
    num = float(num.replace(',', ''))
    ds.set_value(i, 'Amount', num)

ds = ds.drop(['Day', 'BranchName', 'Month'], axis=1)
print("rows: {}, columns: {}".format(ds['Amount'].size, len(list(ds))))

maxValue = ds['Amount'].max()
minValue = ds['Amount'].min()
meanValue = ds['Amount'].mean()
stdValue = ds['Amount'].std()
d = {'max': [maxValue], 'min': [minValue], 'mean': [meanValue], 'std': [stdValue]}
infoTable = pd.DataFrame(data=d)
print(infoTable)

# print(ds)
# ds.to_csv('dataOut.csv')
