import numpy as np
from matplotlib import pyplot
import pandas as pd
from scipy import stats

ds = pd.read_csv('data.csv')
for i in range(ds['Amount'].size):
    num = ds.iloc[i]['Amount'].replace('$', '')
    num = num.replace(',', '')
    ds.set_value(i, 'Amount', (num))
    Transaction_Type = ds.iloc[i]['Transaction_Type']
    if Transaction_Type == 'Cash':
        ds.set_value(i, 'Transaction_Type', 0)
    else:
        ds.set_value(i, 'Transaction_Type', 1)

ds = ds.drop(['Day', 'BranchName', 'Month'], axis=1)
ds = ds.apply(pd.to_numeric)

ds = ds[(np.abs(stats.zscore(ds)) < 3).all(axis=1)]
print(ds)

fig = []
ax = []
for i in range(6):
    fig.append(pyplot.figure(i))
    ax.append(fig[i].gca())

ax[0].hist(ds['Amount'])
ax[1].boxplot(ds['Amount'])
ax[2].scatter(ds['DayWeek'], ds['Amount'])
ax[3].scatter(ds['Hour'], ds['Amount'])
ax[4].scatter(ds['Week'], ds['Amount'])
ax[5].scatter(ds['Units'], ds['Amount'])
pyplot.show()
fig[0].savefig('Amount-HistogramNEW.png')
fig[1].savefig('Amount-BoxPlotNEW.png')
fig[2].savefig('Amount-DayWeek-ScatterNEW.png')
fig[3].savefig('Amount-Hour-BoxPlotNEW.png')
fig[4].savefig('Amount-Week-ScatterNEW.png')
fig[5].savefig('Amount-Units-BoxPlotNEW.png')

ds.to_csv('dataOut.csv')
