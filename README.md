# Implementing Some Data Mining Algorithms (Spring 2019)
- Written in Python, Data Mining course, Dr. Ehsan Nazerfard
- Including KMeans (Also using it in order to compress an image), DBScan, Decision Tree, Random Forest,
Perceptron (rewriting a two-layer neural network) Algorithms