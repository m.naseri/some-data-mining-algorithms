import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import image
import random


def assignment(df, centroids):
    for i in centroids.keys():
        df['distance_from_{}'.format(i)] = (
            np.sqrt(
                (df['R'] - centroids[i][0]) ** 2
                + (df['G'] - centroids[i][1]) ** 2
                + (df['B'] - centroids[i][2]) ** 2
            )
        )
    centroid_distance_cols = ['distance_from_{}'.format(i) for i in centroids.keys()]
    df['closest'] = df.loc[:, centroid_distance_cols].idxmin(axis=1)
    df['closest'] = df['closest'].map(lambda x: int(x.lstrip('distance_from_')))
    return df


def update(centroids):
    for i in centroids.keys():
        centroids[i][0] = np.mean(df[df['closest'] == i]['R'])
        centroids[i][1] = np.mean(df[df['closest'] == i]['G'])
        centroids[i][2] = np.mean(df[df['closest'] == i]['B'])
    return centroids


def kMeans(k, df):
    np.random.seed(200)
    centroids = {
        i: [random.random(), random.random(), random.random()]
        for i in range(k)
    }

    # print(centroids)

    df = assignment(df, centroids)
    iterationCounter = 0
    while True:
        print("KMeans Iteration: {}".format(iterationCounter))
        iterationCounter += 1
        if iterationCounter > maxKMeansIteration:
            break
        closest_centroids = df['closest'].copy(deep=True)
        centroids = update(centroids)
        df = assignment(df, centroids)
        if closest_centroids.equals(df['closest']):
            break
    return centroids


k = 16
maxKMeansIteration = 30
img = image.imread('imageLarge.png')
row = img.shape[0]
column = img.shape[1]
df = pd.DataFrame(img.reshape(-1, 3), columns=list('RGB'))
df.index = np.repeat(np.arange(img.shape[0]), img.shape[1]) + 1
df.index.name = 'Date'

print("Clustering Started.")
centroids = kMeans(k, df)
print("Centroids: ")
print(centroids)
print("Clustering Finished.")

print("Changing Pixels...")
counter = 0
for i in range(row):
    print("{} out of {} rows completed".format(i, row))
    for j in range(column):
        clusterNumber = df.iloc[counter]['closest']
        for k in range(3):
            img[i, j, k] = centroids[clusterNumber][k]
        counter += 1
print("Finished!")

plt.imshow(img)
plt.show()
image.imsave('K{}.png'.format(k), img)
