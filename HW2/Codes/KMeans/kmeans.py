import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def assignment(df, centroids):
    for i in centroids.keys():
        df['distance_from_{}'.format(i)] = (
            np.sqrt(
                (df['x'] - centroids[i][0]) ** 2
                + (df['y'] - centroids[i][1]) ** 2
            )
        )
    centroid_distance_cols = ['distance_from_{}'.format(i) for i in centroids.keys()]
    df['closest'] = df.loc[:, centroid_distance_cols].idxmin(axis=1)
    df['closest'] = df['closest'].map(lambda x: int(x.lstrip('distance_from_')))
    df['color'] = df['closest'].map(lambda x: colmap[x])
    return df


def update(centroids):
    for i in centroids.keys():
        centroids[i][0] = np.mean(df[df['closest'] == i]['x'])
        centroids[i][1] = np.mean(df[df['closest'] == i]['y'])
    return centroids


def kMeans(k, df):
    np.random.seed(200)
    centroids = {
        i: [np.random.randint(df['x'].min(), df['x'].max()),
            np.random.randint(df['y'].min(), df['y'].max())]
        for i in range(k)
    }
    for i in range(15):
        df = assignment(df, centroids)
        # closest_centroids = df['closest'].copy(deep=True)
        centroids = update(centroids)

    if k <= 5:
        fig = plt.figure()
        fig.suptitle('{} K = {}'.format(datasetName, k), fontsize=10)
        plt.scatter(df['x'], df['y'], color=df['color'], alpha=0.5, edgecolor='k')
        for i in centroids.keys():
            plt.scatter(*centroids[i], color='k')
        plt.show()
        fig.savefig('{} K{}.jpg'.format(datasetName, k))

    clusterError = [0 for i in range(k)]
    nOfMemOfCluster = [0 for i in range(k)]
    for i in range(df['x'].size):
        clusterIndex = df.iloc[i]['closest']
        clusterError[clusterIndex] += np.sqrt(
            (df.iloc[i]['x'] - centroids[clusterIndex][0]) ** 2
            + (df.iloc[i]['y'] - centroids[clusterIndex][1]) ** 2)
        nOfMemOfCluster[clusterIndex] += 1

    meanOfClusterError = 0
    # print(clusterError)
    # print(nOfMemOfCluster)
    for i in range(k):
        # print("K: {}, i: {}".format(k, i))
        if nOfMemOfCluster[i] != 0:
            meanOfClusterError += clusterError[i] / nOfMemOfCluster[i]
    meanOfClusterError /= k
    return meanOfClusterError


datasetName = 'Dataset1'
df = pd.read_csv('{}.csv'.format(datasetName))
df.rename(index=str, columns={'X': 'x', 'Y': 'y'}, inplace=True)
fig = plt.figure()
fig.suptitle('{} RawData'.format(datasetName), fontsize=10)
plt.scatter(df['x'], df['y'], color='k')
colmap = {0: 'r', 1: 'g', 2: 'b', 3: 'y', 4: 'm'}
for i in range(5, 15):
    colmap[i] = 'k'

plt.show()
fig.savefig('{}RawData.jpg'.format(datasetName))

Ks = []
mOCEs = []
for k in range(1, 15):
    Ks.append(k)
    mOCEs.append(kMeans(k, df))

fig = plt.figure()
fig.suptitle('{}CE vs K'.format(datasetName), fontsize=10)
plt.plot(Ks, mOCEs)
plt.show()
fig.savefig('{}CEvsK.jpg'.format(datasetName))
