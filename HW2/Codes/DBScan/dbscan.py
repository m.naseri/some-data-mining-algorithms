import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

####################### DBSCAN #########################

datasetName = 'Dataset2'
df = pd.read_csv('{}.csv'.format(datasetName))
X = df.values

print("--------DBCAN--------")
eps = 1.8
minPts = 10

db = DBSCAN(eps=eps, min_samples=minPts).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

print('Estimated number of clusters: %d' % n_clusters_)
print('Estimated number of noise points: %d' % n_noise_)
print("DBSCAN: Silhouette Coefficient: %0.5f"
      % metrics.silhouette_score(X, labels))

unique_labels = set(labels)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
fig = plt.figure()
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    # xy = X[class_member_mask & core_samples_mask]
    # plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
    #          markeredgecolor='k', markersize=14)
    #
    # xy = X[class_member_mask & ~core_samples_mask]
    # plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
    #          markeredgecolor='k', markersize=6)

    xy = X[class_member_mask & core_samples_mask]
    plt.scatter(xy[:, 0], xy[:, 1], color=tuple(col))

    xy = X[class_member_mask & ~core_samples_mask]
    plt.scatter(xy[:, 0], xy[:, 1], color=tuple(col))

plt.title('{} DBSCAN, numOfClusters: {}, '
          'eps: {}, minPts: {}'
          .format(datasetName, n_clusters_, eps, minPts))
plt.show()
fig.savefig('{}DBSCAN_numOfClusters:{}_eps:{}_minPts:{}.jpeg'
            .format(datasetName, n_clusters_, eps, minPts))

####################### K MEANS #########################
print("\n\n--------KMEANS--------")
k = 3
kmeans = KMeans(n_clusters=k)
kmeans.fit(X)
y_kmeans = kmeans.predict(X)

print("K Means: Silhouette Coefficient: %0.5f"
      % metrics.silhouette_score(X, y_kmeans))

fig = plt.figure()
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, cmap='viridis')
centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', alpha=0.5)
plt.title('{} KMeans, numOfClusters: {}'
          .format(datasetName, k))
plt.show()
fig.savefig('{}KMeans_numOfClusters:{}.jpeg'
            .format(datasetName, k))
